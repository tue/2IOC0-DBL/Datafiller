package utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.util.Locale;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Converts a Twitter date string to a DateTime with timezone.
 *
 * @author Peter Wu
 */
public class TwitterDateAdapter implements JsonDeserializer<DateTime>,
        JsonSerializer<DateTime> {
    
    private static final DateTimeFormatter DATE_FORMATTER
            = DateTimeFormat.forPattern("EEE MMM dd HH:mm:ss Z yyyy")
            .withLocale(Locale.ENGLISH)
            .withOffsetParsed();
    
    @Override
    public DateTime deserialize(JsonElement je, Type type,
            JsonDeserializationContext jdc) throws JsonParseException {
        JsonPrimitive jp;
        if (!je.isJsonPrimitive() || !(jp = je.getAsJsonPrimitive()).isString()) {
            throw new JsonParseException("Invalid date type");
        }
        String datetime = jp.getAsString();
        try {
            return DATE_FORMATTER.parseDateTime(datetime);
        } catch (IllegalArgumentException ex) {
            throw new JsonParseException("Invalid date: " + datetime, ex);
        }
    }
    
    @Override
    public JsonElement serialize(DateTime t, Type type,
            JsonSerializationContext jsc) {
        return new JsonPrimitive(formatDateTime(t));
    }
    
    public static String formatDateTime(DateTime t) {
        return DATE_FORMATTER.print(t);
    }
}
