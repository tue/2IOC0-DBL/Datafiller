package io;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

/**
 * Read files from a compressed file.
 *
 * @author Peter Wu
 */
public class CompressedTweetReader extends TweetReader {

    public CompressedTweetReader(String filename) throws IOException {
        super(new GZIPInputStream(new FileInputStream(filename)));
    }

}
