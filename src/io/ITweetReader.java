package io;

import data.Tweet;
import java.io.IOException;

/**
 * Allow you to gather tweet object from some file.
 *
 * @author Peter Wu
 */
public interface ITweetReader {

    public Tweet getTweet() throws IOException;

    public void close();
}
