package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Reads tweets from file.
 *
 * @author Peter Wu
 */
public class FileTweetReader extends TweetReader {

    public FileTweetReader(String filename) throws FileNotFoundException {
        super(new FileInputStream(filename));
    }
}
