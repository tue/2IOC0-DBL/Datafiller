package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Builder for connection.
 *
 * @author Peter Wu
 */
public class ConnectionBuilder {

    private String dbms;
    private String serverName;
    private int port;
    private String dbName;
    private String username;
    private String password;

    /**
     * Sets up a ConnectionBuilder for PostgreSQL and serverName localhost. The
     * username, password and database name must still be supplied.
     */
    public ConnectionBuilder() {
        dbms = "postgresql";
        serverName = "localhost";
        port = 5432;
    }

    public ConnectionBuilder setDbms(String dbms) {
        this.dbms = dbms;
        return this;
    }

    public ConnectionBuilder setServerName(String serverName) {
        this.serverName = serverName;
        return this;
    }

    public ConnectionBuilder setPort(int port) {
        this.port = port;
        return this;
    }

    public ConnectionBuilder setDbName(String dbName) {
        this.dbName = dbName;
        return this;
    }

    public ConnectionBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public ConnectionBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public Connection create() throws SQLException {
        String url = "jdbc:" + dbms + "://" + serverName + ":" + port + "/" + dbName;
        return DriverManager.getConnection(url, username, password);
    }
}
