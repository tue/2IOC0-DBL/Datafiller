package data;

import org.joda.time.DateTime;

/**
 * Represents the tweet object as returned by the twitter API.
 */
public class Tweet {

    public long id;
    @ValidatingJsonDeserializer.Nullable
    public String lang;
    @ValidatingJsonDeserializer.Nullable
    public Long in_reply_to_status_id;
    public DateTime created_at;
    public int favorite_count;
    @ValidatingJsonDeserializer.Nullable
    @ValidatingJsonDeserializer.Validator
    public Place place;
    @ValidatingJsonDeserializer.Nullable
    @ValidatingJsonDeserializer.Validator
    public Coordinates coordinates;
    public String text;
    @ValidatingJsonDeserializer.Nullable
    @ValidatingJsonDeserializer.Validator
    public Tweet retweeted_status;
    @ValidatingJsonDeserializer.Validator
    public Entities entities;
    public long retweet_count;
    @ValidatingJsonDeserializer.Validator
    public User user;

    @Override
    public String toString() {
        return TwitterJsonDeserializer.getGsonBuilder()
                .setPrettyPrinting().create().toJson(this);
    }

    public static class Place {

        //public String id; // "a5b6bdd8008412b1"
        //public String name; // "Danbury"
        //public String country_code; // "US"
        public String country; // "United States"
        //public String url; // "https://api.twitter.com/1.1/geo/id/a5b6bdd8008412b1.json"
        public String full_name; // "Danbury, CT"
    }

    public static class Coordinates {

        //public String type; // always "Point"?
        @ValidatingJsonDeserializer.ArrayValidator(minLen = 2, maxLen = 2)
        public float[] coordinates; // e.g. [-73.49513755, 41.43286284]
    }

    public static class Entities {

        @ValidatingJsonDeserializer.Validator
        public Hashtag[] hashtags;
        @ValidatingJsonDeserializer.Validator
        public Url[] urls;
        @ValidatingJsonDeserializer.Validator
        public Mention[] user_mentions;
    }

    public static class Hashtag {

        public String text;
    }

    public static class Url {

        public String expanded_url;
        //public String display_url;
        //public String url;
    }

    public static class Mention {

        public long id; // user ID
        //public String name; // display name
        //public String screen_name; // Screen name (at-name)
    }
}
