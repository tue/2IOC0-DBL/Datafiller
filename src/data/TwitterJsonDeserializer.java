package data;

import utils.TwitterDateAdapter;
import com.google.gson.GsonBuilder;
import org.joda.time.DateTime;

/**
 * Deserializer for Twitter objects.
 *
 * @author Peter Wu
 */
public class TwitterJsonDeserializer extends ValidatingJsonDeserializer {

    public TwitterJsonDeserializer() {
        super(getBaseGsonBuilder()
                .create());
    }

    private static GsonBuilder getBaseGsonBuilder() {
        return new GsonBuilder()
                .registerTypeAdapter(DateTime.class, new TwitterDateAdapter());
    }

    public static GsonBuilder getGsonBuilder() {
        TwitterJsonDeserializer tjd = new TwitterJsonDeserializer();
        return getBaseGsonBuilder()
                .registerTypeAdapter(Tweet.class, tjd)
                .registerTypeAdapter(User.class, tjd);
    }
}
