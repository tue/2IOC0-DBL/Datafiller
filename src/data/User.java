package data;

import org.joda.time.DateTime;

/**
 * Represents the user object from the Twitter API as contained in a tweet.
 */
public class User {

    public long id;
    public String name;
    @ValidatingJsonDeserializer.Nullable
    public String time_zone;
    public int statuses_count;
    public int followers_count;
    public int friends_count;
    @ValidatingJsonDeserializer.Nullable
    public String location;
    public String screen_name;
    public DateTime created_at;
    @ValidatingJsonDeserializer.Nullable
    @ValidatingJsonDeserializer.Validator
    public Entities entities;
    public String lang;
    @ValidatingJsonDeserializer.Nullable
    public String description;
    public boolean verified;

    @Override
    public String toString() {
        return TwitterJsonDeserializer.getGsonBuilder()
                .setPrettyPrinting().create().toJson(this);
    }

    public static class Entities {

        @ValidatingJsonDeserializer.Nullable
        @ValidatingJsonDeserializer.Validator
        public UrlEntity url;
    }

    public static class UrlEntity {

        @ValidatingJsonDeserializer.Validator
        public Url[] urls;
    }

    public static class Url {

        @ValidatingJsonDeserializer.Nullable
        public String expanded_url;
        //public String display_url;
        //public String url;
    }
}
