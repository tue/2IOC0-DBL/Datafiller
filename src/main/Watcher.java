package main;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * While the DataFiller is working, get some status information.
 *
 * @author Peter Wu
 */
class Watcher implements Runnable {

    private final Main main;
    /**
     * Interval in seconds in which the status information is updated.
     */
    private final int interval;
    private final DateTimeFormatter df;
    private int elapsed_seconds = 0;
    private int previousCount = 0;

    public Watcher(Main main, int interval) {
        this.main = main;
        this.interval = interval;
        df = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
    }

    @Override
    public void run() {
        int newCount = main.getProcessedTweets();
        if (newCount < 1) {
            // database connection is being established? Whatever is the case,
            // no status information should be printed.
            return;
        }
        int tweets_per_min = (newCount - previousCount) * 60 / interval;
        int tweets_per_sec = tweets_per_min / 60;
        elapsed_seconds += interval;
        // tweets per min based on total number of tweets and elapsed time
        int total_tweets_per_min = newCount * 60 / elapsed_seconds;
        String datetime = df.print(new LocalDateTime());
        // store new tweets count for next sample
        previousCount = newCount;
        System.err.println(datetime + " Processed tweets: " + newCount
                + " (total: " + total_tweets_per_min + " tweet/min)"
                + " (last interval: " + tweets_per_sec + " tweets/sec, "
                + tweets_per_min + " tweets/min)");
    }
}
