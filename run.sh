#!/bin/sh
# You should build the jar file with `ant jar`, then run this script

# Exit on errors
set -e

# Change dir to project
cd "$(dirname "$(readlink -f "$0")")"

# java jar is slower. Also note that netbeans might already have built the
# classes so `ant compile` will be faster.
ant -q compile
java -cp build/classes:$(echo lib/*.jar | tr \  :) main.Main "$@"
exit # Do not build jar

jar=dist/Datafiller.jar
# Build jar if missing
[ -e "$jar" ] || ant jar

java -jar "$jar" "$@"
